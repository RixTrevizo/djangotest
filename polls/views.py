from django.shortcuts import render
import json


# Create your views here.
from django.http import HttpResponse
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)


def ajaxTest(request, question_id):
    myDic = {'key': 'argument', 'key1': 'argument1', 'key2': 'argument2', }
    data = json.dumps(myDic)

    return HttpResponse(data)